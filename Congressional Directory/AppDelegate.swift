//
//  AppDelegate.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/15/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit
import CoreLocation
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		// on first launch, we go and download all data
		if (!UserDefaults.standard.bool(forKey: "first"))
		{
			//* this starts a cascading effect where all
			// pieces of data are requested and loaded into
			// Core Data one after the other */
			DataFetcher.requestAllSenators(cascade: true)
			
			UserDefaults.standard.set(true, forKey: FIRST_LAUNCH)
			// this is use to return to the prvious rep they had selected
			UserDefaults.standard.set(true, forKey: PREVIOUS_LAUNCH)
		}
		
		// Have we fetced our facts of the day?!
		if (UserDefaults.standard.object(forKey: FOD_FETCHED) == nil)
		{
			let date = Date()
			//* using the same date makes it easier
			// later when requesting from Core Data as I
			// can refer to the UserDefaults object */
			DataFetcher.fetchFOD(date: date)
			UserDefaults.standard.set(date, forKey: FOD_FETCHED)
		} else
		{
			let lastDate = UserDefaults.standard.value(forKey: FOD_FETCHED) as! Date
			if (!Calendar.current.isDateInToday(lastDate))
			{
				let date = Date()
				DataFetcher.fetchFOD(date: date)
				UserDefaults.standard.set(date, forKey: FOD_FETCHED)
			}
		}
		
		// Jump directly to My rep view (the good stuff). User can go back to hom menu
		// BUT only if we know their homestate otherwise CRASH!
		if (UserDefaults.standard.bool(forKey: STATE_IS_SET))
		{
			// Access the storyboard and fetch an instance of the view controller
			let storyboard = UIStoryboard(name: "Main", bundle: nil);
			let viewController: MyRepViewController = storyboard.instantiateViewController(withIdentifier: "myRepTableVC") as! MyRepViewController
			// Then push that view controller onto the navigation stack
			let rootViewController = self.window!.rootViewController as! UINavigationController
			rootViewController.pushViewController(viewController, animated: true)
		}
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}
