//
//  DatabaseController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/13/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import CoreData

/**
`CDManager` is the main class use to interface with Core Data
and the various data model entities. It has helper functions
to fetch various model entites and the current context.

- Author: Prince Kannah
*/
class CDManager {
    
    private init(){ /** can't be called directly **/ }
	
	/**
	Retrieve the managed object context from the container.
	- Returns: the manaaged context.
	*/
    class func context() -> NSManagedObjectContext {
        return persistentContainer.viewContext
    }

	/**
	Retrieve the `Office` entity with the specified ID.
	- Parameter officeID: the `Office` entity ID.
	- Returns: `Office` with the specified ID.
	*/
	class func fetchOffice (_ officeID: String) -> Office {
		var selected: Office!
		do {
			let request: NSFetchRequest<Office> = Office.fetchRequest()
			let predicate = NSPredicate(format: "officeID = %@", officeID)
			request.predicate = predicate
			let results = try CDManager.context().fetch(request)
			if (results.count > 0) {
				selected = results[0]
			}
		} catch {
			fatalError("[CDManager]Unable to fetch Office from Core Data: \(error)")
		}
		return selected
	}
	
	/**
	Retrieve the `CongressMember` with the specified ID.
	- Parameter apiID: the member's ProPublica Congress API ID.
	- Returns: member with the specified ID.
	*/
	class func fetchMember (_ id: String) -> CongressMember {
		var selected: CongressMember!
		do {
			let request: NSFetchRequest<CongressMember> = CongressMember.fetchRequest()
			let predicate = NSPredicate(format: "apiID = %@", id)
			request.predicate = predicate
			let results = try CDManager.context().fetch(request)
			if (results.count > 0) {
				selected = results[0]
			}
		} catch {
			fatalError("[CDManager]Unable to fetch CongressMember from Core Data: \(error)")
		}
		return selected
	}
	
	/**
	Retrieve the `Committee` with the specified ID.
	- Parameter committeeID: the committe's ID.
	- Returns: committe with the specified ID.
	*/
	class func fetchCommittee (_ committeeID: String) -> Committee {
		var selected: Committee!
		do {
			let request: NSFetchRequest<Committee> = Committee.fetchRequest()
			let predicate = NSPredicate(format: "committeeID = %@", committeeID)
			request.predicate = predicate
			let results = try CDManager.context().fetch(request)
			if (results.count > 0) {
				selected = results[0]
			}
		} catch {
			fatalError("[CDManager]Unable to fetch Committee from Core Data: \(error)")
		}
		return selected
	}
	
    // MARK: - Core Data stack
    
    static var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Congressional_Directory")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate.
				// You should not use this function in a shipping application,
				// although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    class func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate.
				// You should not use this function in a shipping application,
				// although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
