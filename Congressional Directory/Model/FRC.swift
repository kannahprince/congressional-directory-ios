//
//  FRC.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/17/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import CoreData

class FRC: NSObject, NSFetchedResultsControllerDelegate {
	class func initFRC(_ entity: String, _ sortDescriptors: [NSSortDescriptor], _ predicate:NSPredicate, _ delegate: NSFetchedResultsControllerDelegate) -> NSFetchedResultsController<NSFetchRequestResult> {
        var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        request.sortDescriptors = sortDescriptors
        request.predicate = predicate
        
        let moc = CDManager.context()
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: sortDescriptors[0].key, cacheName: nil)
        fetchedResultsController.delegate = delegate
        
        do {
            try fetchedResultsController.performFetch()
            return fetchedResultsController
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
}
