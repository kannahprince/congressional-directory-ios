//
//  SecondViewController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/15/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit
import CoreData

class MemberCommitteesView: UITableViewController, NSFetchedResultsControllerDelegate {
	private let CELL_ID = "detailCommittees"
	var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		initializeFetchedResultsController()
		tableView.tableFooterView = UIView()
	}
	
	func initializeFetchedResultsController() {
		let request = NSFetchRequest<NSFetchRequestResult>(entityName: COMMITTEE_ENTITY)
		let sortBy = NSSortDescriptor(key: "committeeID", ascending: true)
		let predicate = NSPredicate(format: "ANY members.apiID = %@", "\(SELECTED_MEMBER_ID)")
		request.sortDescriptors = [sortBy]
		request.predicate = predicate
		
		let moc = CDManager.context()
		fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: "type", cacheName: nil)
		fetchedResultsController.delegate = self
		
		do {
			try fetchedResultsController.performFetch()
		} catch {
			fatalError("[MemberCommitteesView]Failed to initialize FetchedResultsController: \(error)")
		}
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID, for: indexPath)
		// Set up the cell
		guard let object = self.fetchedResultsController?.object(at: indexPath) else {
			fatalError("[MemberCommitteesView]Attempt to configure cell without a managed object")
		}
		
		let committee = object as! Committee
		cell.textLabel?.text = committee.name
		if (committee.chairID == SELECTED_MEMBER_ID) {
			cell.detailTextLabel?.text = "Chair"
		} else {
			cell.detailTextLabel?.text = ""
		}
		
		if (committee.rankingMemberID == SELECTED_MEMBER_ID){
			cell.detailTextLabel?.text = "Ranking Member"
		} else {
			cell.detailTextLabel?.text = ""
		}
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let selectedCommittee = fetchedResultsController.object(at: indexPath) as! Committee
		SELECTED_COMMITTE_ID = selectedCommittee.committeeID!
		
		// present the details of the selected committee
		let destStoryboard: UIStoryboard = UIStoryboard(name: "CommitteeDetail", bundle: nil)
		let destination = destStoryboard.instantiateViewController(withIdentifier: "committeeTBController")
		self.navigationController?.pushViewController(destination, animated: true)
	}
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		guard let sections = fetchedResultsController.sections else {
			fatalError("[MemberCommitteesView]No sections in fetchedResultsController")
		}
		let sectionInfo = sections[section]
		return sectionInfo.name
	}
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return fetchedResultsController.sections!.count
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let sections = fetchedResultsController.sections else {
			fatalError("[MemberCommitteesView]No sections in fetchedResultsController")
		}
		let sectionInfo = sections[section]
		return sectionInfo.numberOfObjects
	}
	
}
