//
//  ContactsMapVC.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 12/8/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit
import MapKit

class ContactsMapVC: UIViewController {
	let REGION_RADIUS: CLLocationDistance = 1000
	@IBOutlet weak var mapView: MKMapView!
	
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
		loadPins()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	func loadPins() {
		let member:CongressMember = CDManager.fetchMember(SELECTED_MEMBER_ID)
		let offices = member.offices!
		for (index, office) in offices.enumerated() {
			let location = office as! Office
			let lat = location.latitude
			let long = location.longitude
			let coordinate:CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat, long)
			let officePin = MKPointAnnotation()
			officePin.coordinate = coordinate
			officePin.title = "District office \(index + 1)"
			mapView.addAnnotation(officePin)
		}
	}
	
	func centerMapOnLocation(location: CLLocation) {
		let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
																  REGION_RADIUS * 3.5, REGION_RADIUS * 3.5)
		mapView.setRegion(coordinateRegion, animated: true)
	}

	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
