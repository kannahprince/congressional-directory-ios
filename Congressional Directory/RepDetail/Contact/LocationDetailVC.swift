//
//  LocationDetailVC.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 12/9/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit

class LocationDetailVC: UIViewController {

	@IBOutlet weak var textView: UITextView!
	override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
		let office:Office = CDManager.fetchOffice(SELECTED_OFFICE_ID)
		self.title = office.city!
		var addressString = office.address!
		if (office.suite != nil) {
			addressString.append("\n\(office.suite!)")
			addressString.append("\n\(office.city!), \(office.state!) \(office.zipcode!)")
		} else {
			addressString.append("\n\(office.city!), \(office.state!) \(office.zipcode!)")
		}
		
		if (office.hours != nil) {
			addressString.append("\n\n\(office.hours!)")
		} else {
			addressString.append("\n\nLocation hours unavailable.")
		}
		
		if (office.phoneNumber != nil) {
			addressString.append("\n\nPhone: \(office.phoneNumber!)")
		} else {
			addressString.append("\n\nPhone number unvailable.")
		}
		
		if (office.faxNumber != nil) {
			addressString.append("\nFax: \(office.faxNumber!)")
		} else {
			addressString.append("\nFax number unavailable.")
		}
		textView.text = addressString
    }
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
