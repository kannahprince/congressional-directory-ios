//
//  CongressionalWebsiteVC.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 12/10/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit
import WebKit

class CongressionalWebsiteVC: UIViewController, UIWebViewDelegate {
	var memberContactFromString = ""
	override func viewDidLoad() {
		let webView:UIWebView = UIWebView()
		self.view = webView
		view.updateConstraints()
		webView.delegate = self
		let myURL = URL(string: memberContactFromString)
		let request:URLRequest = URLRequest(url: myURL!)
//		UIApplication.shared.openURL(URL(string: "http://www.stackoverflow.com")!)
		UIApplication.shared.open(myURL!, options: [:]) { _ in }
		webView.loadRequest(request)
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
