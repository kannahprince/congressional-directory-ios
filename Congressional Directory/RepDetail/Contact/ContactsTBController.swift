//
//  ContactsTBController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 12/8/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit

class ContactsTBController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
		// the added space is for members with long last name
		self.title = " District offices"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
