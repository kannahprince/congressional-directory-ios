//
//  FirstViewController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/15/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit
import CoreData

class BiographyViewController: UIViewController {
	@IBOutlet weak var textView: UITextView!
	override func viewDidLoad() {
        super.viewDidLoad()
		var bio = ""
		let member:CongressMember = CDManager.fetchMember(SELECTED_MEMBER_ID)
		if (member.bio == nil) {
			bio = "No biographical information currently available for this representative."
			bio += "\n\nNext election: \(member.nextElection!)"
		} else {
			bio = member.bio!
			bio += "\n\nNext election: \(member.nextElection!)"
		}
		self.textView.text = bio
    }
}
