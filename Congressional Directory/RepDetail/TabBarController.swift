//
//  TabBarController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/12/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import UIKit

class TabBarController: UITabBarController {
    override func viewDidLoad() {
		let cong:CongressMember = CDManager.fetchMember(SELECTED_MEMBER_ID)
		if (cong.title == "Representative") {
			let rep = cong as! Representative
			self.title = rep.shortTitle! + " " + rep.lastName!
		} else {
			let sen = cong as! Senator
			self.title = sen.shortTitle! + " " + sen.lastName!
		}
    }
}

