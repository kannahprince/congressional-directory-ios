//
//  LegislationViewController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 12/6/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit
import CoreData
class LegislationViewController: UITableViewController {
	let SECTION_TITLES = ["At a glance", "Bills introduced for current session"]
	let AT_A_GLANCE_SECTION_NUM = 0
	let AT_A_GLANCE_NUM_ROW = 7
	
	let LEGISLATION_CELL_ID = "legislations"
	var bills: [Bill]!
	override func viewDidLoad() {
        super.viewDidLoad()
		initBillsSponsored()
//		DataFetcher.getLegislation(memberID: SELECTED_MEMBER_ID, billType: BILLS_INTRODUCED)
		tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }

	func initBillsSponsored() {
		do {
			let request: NSFetchRequest<Bill> = Bill.fetchRequest()
			let predicate = NSPredicate(format: "sponsorID = %@ AND congress = %@", "\(SELECTED_MEMBER_ID)", "\(CURRENT_CONGRESS)")
			let descriptor = NSSortDescriptor(key: "title", ascending: false)
			request.predicate = predicate
			request.sortDescriptors = [descriptor]
			bills = try CDManager.context().fetch(request)
		} catch  {
			fatalError("[LegislationViewController(initBilsSponsored]Fail to fetch bills")
		}
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let member:CongressMember = CDManager.fetchMember(SELECTED_MEMBER_ID)
		switch indexPath.section {
		case 0:
			let cell = tableView.dequeueReusableCell(withIdentifier: "atAGlance", for: indexPath)
			return updateAtAGlanceCell(cell, indexPath, member)
		default:
			let cell = tableView.dequeueReusableCell(withIdentifier: LEGISLATION_CELL_ID, for: indexPath)
			let bill = bills[indexPath.row]
			cell.textLabel?.text = bill.shortTitle
			cell.detailTextLabel?.text = "Introduced on \(prettyDate(date: bills[indexPath.row].introducedOn!))"
			return cell
		}
	}
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return SECTION_TITLES[section]
	}
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 2
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if (section == 0) {
			return AT_A_GLANCE_NUM_ROW
		}
		return bills.count
	}
	
//	override func sectionIndexTitlesForTableView(tableView: UITableView) -> [AnyObject]! {
//		return indexOfNumbers
//	}
	
//	override func tableView(tableView: UITableView, sectionForSectionIndexTitle title: String, atIndex index: Int) -> Int {
//		var temp = indexOfNumbers as NSArray
//		return temp.indexOfObject(title)
//	}
	
	func updateAtAGlanceCell (_ cell: UITableViewCell, _ indexPath: IndexPath, _ member: CongressMember) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "atAGlance", for: indexPath)
		switch indexPath.row {
		case 0:
			cell.textLabel?.text = "Votes with party"
			cell.detailTextLabel?.text = "\(member.votesWithPartyPct)%"
		case 1:
			cell.textLabel?.text = "Total votes"
			cell.detailTextLabel?.text = "\(member.totalVotes)"
		case 2:
			cell.textLabel?.text = "Total present (neither YES or NO)"
			cell.detailTextLabel?.text = "\(member.totalPresent)"
		case 3:
			cell.textLabel?.text = "Missed votes"
			cell.detailTextLabel?.text = "\(member.missedVotes)"
		case 4:
			cell.textLabel?.text = "Missed vote percentage"
			cell.detailTextLabel?.text = "\(member.missedVotesPct)%"
		case 5:
			cell.textLabel?.text = "Total bills sponsored"
			cell.detailTextLabel?.text = "\(member.billsSponsored)"
		case 6:
			cell.textLabel?.text = "Total bills cosponsored"
			cell.detailTextLabel?.text = "\(member.billsCoSponsored)"
		default: break
		}
		return cell
	}
	
	// return easy human reader date
	func prettyDate (date: Date) -> String {
		let formatter = DateFormatter()
		var returnString = ""
		formatter.dateStyle = .short
		formatter.dateFormat = "YYYY-MM-DD"
		let dateString = formatter.string(from: date).components(separatedBy: "-")
		
		switch dateString[1] {
		case "01":
			// build pretty string Month Date, Year
			returnString = "\(formatter.shortMonthSymbols[0]) \(dateString[2]), \(dateString[0])"
		case "02":
			returnString = "\(formatter.shortMonthSymbols[1]) \(dateString[2]), \(dateString[0])"
		case "03":
			returnString = "\(formatter.shortMonthSymbols[2]) \(dateString[2]), \(dateString[0])"
		case "04":
			returnString = "\(formatter.shortMonthSymbols[3]) \(dateString[2]), \(dateString[0])"
		case "05":
			returnString = "\(formatter.shortMonthSymbols[4]) \(dateString[2]), \(dateString[0])"
		case "06":
			returnString = "\(formatter.shortMonthSymbols[5]) \(dateString[2]), \(dateString[0])"
		case "07":
			returnString = "\(formatter.shortMonthSymbols[6]) \(dateString[2]), \(dateString[0])"
		case "08":
			returnString = "\(formatter.shortMonthSymbols[7]) \(dateString[2]), \(dateString[0])"
		case "09":
			returnString = "\(formatter.shortMonthSymbols[8]) \(dateString[2]), \(dateString[0])"
		case "10":
			returnString = "\(formatter.shortMonthSymbols[9]) \(dateString[2]), \(dateString[0])"
		case "11":
			returnString = "\(formatter.shortMonthSymbols[10]) \(dateString[2]), \(dateString[0])"
		case "12":
			returnString = "\(formatter.shortMonthSymbols[11]) \(dateString[2]), \(dateString[0])"
		default: break
		}
		return returnString
	}

}
