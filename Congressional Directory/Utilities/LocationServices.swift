//
//  LocationManager.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/17/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import CoreLocation

class LocationServices: NSObject, CLLocationManagerDelegate {
    private static let locationManager = CLLocationManager()
    private static let delegate:LocationServices = LocationServices()
	
	class func startGPS() {
		locationManager.delegate = delegate
		locationManager.desiredAccuracy = kCLLocationAccuracyBest
		locationManager.requestWhenInUseAuthorization()
		locationManager.startUpdatingLocation()
	}
	
	
	class func initLocation()
	{
        switch CLLocationManager.authorizationStatus()
		{
        case .notDetermined:
            // Request when-in-use authorization initially
            locationManager.requestWhenInUseAuthorization()
		case .restricted, .denied: break
            // Disable location features; show input filed for state initial
        case .authorizedWhenInUse:
            // Enable basic location features;
            // Do a reverse geocoding to get user's state abreviation
			reverseGeoCoder()
		case .authorizedAlways: break
            // Enable any of your app's location features; set up geo fence to
            // show current location's rep
            //enableMyAlwaysFeatures()
        }
    }
	
	class func reverseGeoCoder() {
		let ceo = CLGeocoder()
		let coordinate = locationManager.location?.coordinate
		let loc = CLLocation(latitude: (coordinate?.latitude)!, longitude: (coordinate?.longitude)!)
		ceo.reverseGeocodeLocation(loc) { placemarks, error in
			// if false HOME_STATE_IS_SET will will also be false,
			// causing the prompt to show when home screen loads
			if (error == nil)
			{
				let placemark = placemarks?.first
				let stateAbre = placemark?.administrativeArea!
				print(stateAbre!)
				// update home state
				UserDefaults.standard.setValue(stateAbre, forKey: STATE_ABREV)
				UserDefaults.standard.set(true, forKey: STATE_IS_SET)
			}
		}
	}
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
			switch status {
				case .restricted, .denied:
				// Disable your app's location features
				//disableMyLocationBasedFeatures()
				break
				case .authorizedWhenInUse:
				// Enable only your app's when-in-use features.
				//enableMyWhenInUseFeatures()
				break
				case .authorizedAlways:
				// Enable any of your app's location services.
			   // enableMyAlwaysFeatures()
				break
				case .notDetermined:
				break
			}
    }
}
