//
//  Globals.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/17/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import SwiftyJSON

// MARK: - API END POINTS & related -> API end points and related constants

/**
The base URL for ProPublica Congress API
*/
let API_BASE_URL			= "https://api.propublica.org/congress/"

/**
The current ProPublica Congress API version
*/
let CURRENT_API_VERSION	= "v1"

/**
The current session of Congress
*/
let CURRENT_CONGRESS		= 115

/**
The ProPublica Congress API Senate endpoint
*/
let SENATE				= "senate"

/**
The ProPublica Congress API House of Representative endpoint
*/
let HOUSE				= "house"

/**
A string value in response JSON indicating whether a House member
is representing an At-large district (i.e. usually the state
or US territory). At-Large district members do not have a district number.
*/
let DISTRICT_AT_LARGE	= "At-Large"

/**
The ProPublica Congress API bills introduced by
a member end point.
*/
let BILLS_INTRODUCED		= "introduced"

/**
The ProPublica Congress API bills cosponsored
by a member end point.
*/
let BILLS_COSPONSORED	= "cosponsored"

/**
The ProPublica Congress API bills updated by
a member end point.
*/
let BILLS_UPDATED		= "updated"

/**
The ProPublica Congress API joint committees end point
*/
let JOINT_ENDPOINT		= "joint"

/**
The base S3 bucket URL containing headshots for each member of Congress.
*/
let IMAGE_BASE_URL		= "https://s3-us-west-2.amazonaws.com/congressioandirectory/profilePics/"

/**
The S3 bucket URL containing the biography
JSON for each member of Congress.
*/
let BIO_BASE_URL			= "https://s3-us-west-2.amazonaws.com/congressioandirectory/legislators-current-bio.json"


// other possible value is 0, meaning not in office
let IS_IN_OFFICE = 1

// MARK: - MODEL ENTITIES -> name of Core Data model entities

/**
The name of the Core Data `Bill` entity.
*/
let BILL_ENTITY			= "Bill"

/**
The name of the Core Data `Committee` entity.
*/
let COMMITTEE_ENTITY 	= "Committee"

/**
The name of the Core Data `Subcommittee` entity.
This entity is a child of `Committee`.
*/
let SUBCOMMITTEE_ENTITY 	= "Subcommittee"

/**
The name of the Core Data `CongressMember` entity.
*/
let MEMBER_ENTITY 		= "CongressMember"

/**
The name of the Core Data `Senator` entity.
This entity is a child of `CongressMember`.
*/
let SENATOR_ENTITY 		= "Senator"

/**
The name of the Core Data `Representative` entity.
This entity issa child of `CongressMember`.
*/
let REP_ENTITY 			= "Representative"

/**
The name of the Core Data `Contact` entity.
*/
let CONTACT_ENTITY 		= "Contact"

/**
The name of the Core Data `Office` entity.
*/
let OFFICE_ENTITY 		= "Office"

/**
The name of the Core Data Fact of the Day or (`FOD`) entity.
*/
let FOD_ENTITY 			= "FOD"

// MARK: - USER DEFAULTS KEY-NAME -> The key names of values stored in `UserDefaults`
let COMMITTEES_MEMBERSHIP	= "committeeMembershipsUpdated"
let STATE_IS_SET 			= "homeStateSet"
let STATE_ABREV 				= "homeState"
let FOD_FETCHED 				= "fod"
let FIRST_LAUNCH 			= "first"
let PREVIOUS_LAUNCH 			= "previousLaunch"

// MARK: - SELECTION ID -> the IDs of items selected by user

/**
A global reference to the ID of the currently
selected committee.
*/
var SELECTED_COMMITTE_ID  = ""

/**
A global reference to the ID of the currently
selected member.
*/
var SELECTED_MEMBER_ID    = ""

/**
A global reference to the ID of the currently
selected district office.
*/
var SELECTED_OFFICE_ID    = ""

// MARK: - STATE DICTIONARY -> state name and corresponding state abbreviation

/**
A dictionary key with a state's full name
and the value corresponding to its abbreviation.
*/
let STATE_DICT: [String: String] = [
	"Alaska" 			: "AK",
	"Alabama" 		: "AL",
	"Arkansas" 		: "AR",
	"Arizona" 		: "AZ",
	"California"		: "CA",
	"Colorado"		: "CO",
	"Connecticut" 		: "CT",
	"Delaware" 		: "DE",
	"Florida"			: "FL",
	"Georgia"			: "GA",
	"Hawaii"			: "HI",
	"Iowa"			: "IA",
	"Idaho"			: "ID",
	"Illinois" 		: "IL",
	"Indiana"			: "IN",
	"Kansas" 			: "KS",
	"Kentucky"		: "KY",
	"Louisiana" 		: "LA",
	"Massachusetts"	: "MA",
	"Maryland"		: "MD",
	"Maine"			: "ME",
	"Michigan"		: "MI",
	"Minnesota"		: "MN",
	"Missouri" 		: "MO",
	"Mississippi"		: "MS",
	"Montana"			: "MT",
	"North Carolina" 	: "NC",
	"North Dakota" 	: "ND",
	"Nebraska"		: "NE",
	"New Hampshire" 	: "NH",
	"New Jersey"		: "NJ",
	"New Mexico"		: "NM",
	"Nevada" 			: "NV",
	"New York" 		: "NY",
	"Ohio"			: "OH",
	"Oklahoma"		: "OK",
	"Oregon" 			: "OR",
	"Pennsylvania"		: "PA",
	"Rhode Island" 	: "RI",
	"South Carolina" 	: "SC",
	"South Dakota"		: "SD",
	"Tennessee"		: "TN",
	"Texas" 			: "TX",
	"Utah" 			: "UT",
	"Virginia"		: "VA",
	"Vermont" 		: "VT",
	"Washington" 		: "WA",
	"Wisconsin" 		: "WI",
	"West Virginia" 	: "WV",
	"Wyoming" 		: "WY",
	"American Samoa" 	: "AS",
	"District Of Columbia": "DC",
	"Guam" 			: "GU",
	"Northern Mariana Islands": "MP",
	"Puerto Rico" 		: "PR",
	"Virgin Islands" 	: "VI"
]

/**
Building details for each of the Congressional office
buildings located in Washington, DC. Details include
address, and GPS coordinates retrieved
[using this](https://www.gps-coordinates.net/)
*/
let OFFICE_BUILDINGS: JSON = [
	"RUSSEL"		:
		[
			"address"		:["street"	: "2 Constitution Ave NE", "zip":"20002"],
			"coordinates"	:["latitude"	:38.892461, "longitude":-77.007455]
		],
	"HART"		:
		[
			"address"		: ["street"	: "2nd St NW & Constitution Ave NE", "zip": "20001"],
			"coordinates"	: ["latitude"	: 38.892837, "longitude":-77.004311]
		],
	"LONGWORTH"	:
		[
			"address"		:["street"	:"201 New Jersey Ave SE", "zip":"20003"],
			"coordinates"	:["latitude"	:38.886782, "longitude":-77.008415]
		],
	"DIRKSEN"		:
		[
			"address"		:["street"	: "201 1st St NE", "zip":"20002"],
			"coordinates"	:["latitude" 	:38.892802, "longitude":-77.005209]
		],
	"CANNON"		:
		[
			"address"		:["street"	:"200 New Jersey Ave SE", "zip":"20515"],
			"coordinates"	:["latitude" 	:38.886804, "longitude":-77.007004]
		],
	"RAYBURN":
		[
			"address"		:["street"	: "50 Independence Ave SW" , "zip": "20515"],
			"coordinates"	:["latitude" 	:38.886757, "longitude":-77.010598]
		]
]

