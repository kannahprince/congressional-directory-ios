//
//  DataFetcher.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/14/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//  All API docs can be found here:
//  https://projects.propublica.org/api-docs/congress-api/
//


import Foundation
import CoreData
import Alamofire
import SwiftyJSON
import SwiftSoup

private let API_KEY					= "rm8AfuIG7M7p0W2bLSAw6gFpvooWjiw6iNSdDLqQ"
private let requestHeader: HTTPHeaders 	= [ "X-API-Key": API_KEY ]
private let DISTRICT_OFFICES_URL 		= "https://theunitedstates.io/congress-legislators/legislators-district-offices.json"

/**
The `DataFetcher` class contains code to get data
that is presented in the app. It has methods to
get member data as well Fact of the Day (`FOD`).
Requsts are made using `Alamofire` and the response
are parsed using `SwiftJSON` and stored in `CoreData`
in all cases.

- Note:
	All functions in this class are performed in the
	background using
[persistentContainer.performBackgroundTask()](https://developer.apple.com/documentation/coredata/nspersistentcontainer/1640564-performbackgroundtask)
	this allows for a fluid UI when data is being fetched and
	stored in CoreData.

- Author:
	Prince Kannah
*/
class DataFetcher {
	class func getBio(cascade: Bool = false)
	{
		CDManager.persistentContainer.performBackgroundTask()
			{ _ in
			Alamofire.request(BIO_BASE_URL).responseJSON
				{ response in
				if let responseObject = response.result.value
				{
					let json = JSON(responseObject)
					let results = json["results"].arrayValue
					do {
						let request:NSFetchRequest<CongressMember> = CongressMember.fetchRequest()
						let fetchedResults = try CDManager.context().fetch(request)
						for member in results
						{
							let bio = JSON(member)
							let id = bio["member_id"].stringValue
							for cp in fetchedResults
							{
								if(cp.apiID! == id)
								{
									cp.bio = bio["bio"].stringValue
								}
							}
						}
						CDManager.saveContext()
						switch cascade{
						case true:
							getOffices(cascade: cascade)
						default: break
						}
					} catch {
						fatalError("Unable to fetch members to update biography informaiton \(error)")
					}
				}
			}
		}
	}

	/**
	Retrieve and store in `CoreData` (as `Office` entity) the district offices
	of all current members of Congress.
	
	- Parameter cascade: Whether to request the next set of data
	(`requestAllCommitteesFor(chamber: SENATE_ENDPOINT, cascade: cascade`)
	once the call for district offices is complete. Default is `false`
	*/
	class func getOffices(cascade: Bool = false)
	{
		CDManager.persistentContainer.performBackgroundTask()
			{ _ in
			Alamofire.request(DISTRICT_OFFICES_URL).responseJSON
				{ response in
				if let reponseJSON = response.result.value
				{
					let json = reponseJSON as! NSArray
					for object in json
					{
						let objectAsJSON = JSON(object)
						let id = objectAsJSON["id", "bioguide"].stringValue
						let offices = objectAsJSON["offices"].arrayValue
						do {
							let request:NSFetchRequest<CongressMember> = CongressMember.fetchRequest()
							let predicate = NSPredicate(format: "apiID = %@", id)
							request.predicate = predicate
							let member = try CDManager.context().fetch(request)[0]
							
							for location in offices
							{
								let memberOffice = Office(context: CDManager.context())
								memberOffice.officeID = location["id"].stringValue
								memberOffice.address = location["address"].stringValue
								
								if (!location["suite"].stringValue.isEmpty) {
									memberOffice.suite = location["suite"].stringValue
								}
								memberOffice.city = location["city"].stringValue
								memberOffice.state = location["state"].stringValue
								memberOffice.zipcode = location["zip"].stringValue
								if (!location["hours"].stringValue.isEmpty) {
									memberOffice.hours = location["hours"].stringValue
								}
								
								if (!location["phone"].stringValue.isEmpty) {
									memberOffice.phoneNumber = location["phone"].stringValue
								}
								
								if (!location["fax"].stringValue.isEmpty) {
									memberOffice.faxNumber = location["fax"].stringValue
								}
								
								memberOffice.longitude = location["longitude"].doubleValue
								memberOffice.latitude = location["latitude"].doubleValue
								member.addToOffices(memberOffice)
								memberOffice.officeOf = member
							}
						} catch {
							fatalError("[DataFetcher(getOffices)]Failed to fetch member to update offices: \(error)")
						}
					}
					CDManager.saveContext()
					requestAllCommitteesFor(chamber: SENATE, cascade: cascade)
				}
			}
		}
	}
	
	/**
	Retrieve and store all current Congressional Senators.
	
	- Parameter cascade: Whether to request the next set of data
	(`requestAllRepresentatives(cascade: cascade`)
	once the call for Senators is complete. Default is `false`
	*/
	class func requestAllSenators(cascade: Bool = false)
	{
		let requestString = RequestStringGenerator.craftRequestForAllMembers(SENATE)
		CDManager.persistentContainer.performBackgroundTask()
			{ _ in
            Alamofire.request(requestString, headers: requestHeader).responseJSON
				{ response in
                if let responseJSON = response.result.value
				{
					let json = JSON(responseJSON)
					// get results array, index 0 is the dict, get the members array
					// from in the dict
					let members = json["results", 0, "members"].arrayValue
					
					for member in members
					{
						let senator = Senator(context: CDManager.context())
						let memberDetails = JSON(member)
						senator.apiID = memberDetails["id"].stringValue
						senator.apiURI = memberDetails["api_uri"].stringValue
						senator.firstName = memberDetails["first_name"].stringValue
						senator.middleName = memberDetails["middle_name"].stringValue
						senator.lastName = memberDetails["last_name"].stringValue
						senator.party = memberDetails["party"].stringValue
						senator.stateAbbreviation = memberDetails["state"].stringValue
						senator.nextElection = memberDetails["next_election"].stringValue
						senator.title = "Senator"
						
						if(memberDetails["in_office"].int == IS_IN_OFFICE)
						{
							senator.inOffice = true
							// otherwise defaults to false
						}
						senator.totalVotes = Int16(memberDetails["total_votes"].intValue)
						senator.missedVotes = Int16(memberDetails["missed_votes"].intValue)
						senator.totalPresent = Int16(memberDetails["total_present"].intValue)
						senator.votesWithPartyPct = memberDetails["votes_with_party_pct"].doubleValue
						senator.missedVotesPct = memberDetails["missed_votes_pct"].doubleValue
						
						// senate specific details
						senator.stateRank = memberDetails["state_rank"].stringValue
						senator.seniority = Int16(memberDetails["seniority"].intValue)
						senator.senateClass = Int16(memberDetails["senate_class"].intValue)
					}
					CDManager.saveContext()
					// start the next batch import by getting all House members
					switch cascade {
					case true:
						requestAllRepresentatives(cascade: cascade)
					default: break
					}
				}
			}
		}
    }
	
	/**
	Retrieve and store all current Congressional Representatives.
	
	- Parameter cascade: Whether to request the next set of data
	(`getBio(cascade: cascade)`)
	once the call for Representatives is complete. Default is `false`
	*/
	class func requestAllRepresentatives(cascade: Bool = false)
	{
		let requestString = RequestStringGenerator.craftRequestForAllMembers(HOUSE)
		CDManager.persistentContainer.performBackgroundTask()
			{ _ in
			Alamofire.request(requestString, headers: requestHeader).responseJSON { response in
				if let responseJSON = response.result.value
				{
					let json = JSON(responseJSON)
					// get results array, index 0 is the dict, get the members array
					// from in the dict
					let members = json["results", 0, "members"].arrayValue
					for member in members
					{
						let rep = Representative(context: CDManager.context())
						let memberDetails = JSON(member)
						rep.apiID = memberDetails["id"].stringValue
						rep.apiURI = memberDetails["api_uri"].stringValue
						rep.firstName = memberDetails["first_name"].stringValue
						rep.middleName = memberDetails["middle_name"].stringValue
						rep.lastName = memberDetails["last_name"].stringValue
						rep.party = memberDetails["party"].stringValue
						rep.stateAbbreviation = memberDetails["state"].stringValue
						rep.title = "Representative"
						rep.nextElection = memberDetails["next_election"].stringValue
						if(memberDetails["in_office"].int == IS_IN_OFFICE){
							rep.inOffice = true
							// otherwise defaults to false
						}
						
						if(memberDetails["district"].stringValue == DISTRICT_AT_LARGE){
							rep.districtAtLarge = true
						}else {
							rep.district = Int16(memberDetails["district"].intValue)
						}
						rep.totalVotes = Int16(memberDetails["total_votes"].intValue)
						rep.missedVotes = Int16(memberDetails["missed_votes"].intValue)
						rep.totalPresent = Int16(memberDetails["total_present"].intValue)
						rep.votesWithPartyPct = memberDetails["votes_with_party_pct"].doubleValue
						rep.missedVotesPct = memberDetails["missed_votes_pct"].doubleValue
					}
					CDManager.saveContext()
					switch cascade {
					case true:
						getBio(cascade: cascade)
					default: break
					}
				}
			}
		}
	}
	
	class func updateInfoFor(member: CongressMember)
	{
		CDManager.persistentContainer.performBackgroundTask()
			{ _ in
			Alamofire.request(member.apiURI!, headers: requestHeader).responseJSON { response in
				if let responseObject = response.result.value
				{
					let json = JSON(responseObject)
					let results = JSON(json["results", 0].dictionaryObject!)
					let contact = Contact(context: CDManager.context())
					
					if (!results["facebook_account"].stringValue.isEmpty) {
						contact.facebookAccount = results["facebook_account"].stringValue
					}
					
					if(!results["rss_url"].stringValue.isEmpty) {
						contact.rssURL = results["rss_url"].stringValue
					}
					
					if(!results["twitter_account"].stringValue.isEmpty) {
						contact.twitterAccount = results["twitter_account"].stringValue
					}
					
					if(!results["url"].stringValue.isEmpty) {
						contact.website = results["url"].stringValue
					}
					
					if(!results["youtube_account"].stringValue.isEmpty) {
						contact.youtubeAccount = results["youtube_account"].stringValue
					}
					
					contact.contactsOf = member
					member.contacts = contact
					// get role for current session of congress
					let role = JSON(results["roles", 0].dictionaryObject!)
					// try to get office number from address
					let room = role["office"].stringValue.components(separatedBy: " ")[0]
					if (!role["contact_form"].stringValue.isEmpty) {
						contact.contactForm = role["contact_form"].stringValue
					}
					// Washington, DC office
					let office 			= Office(context: CDManager.context())
					office.city 			= "Washington"
					office.suite 		= "Suite \(room)"
					office.faxNumber 		= role["fax"].stringValue
					
					if (member.title == "Representative")
					{
						// figure which building they're in and assign appropritate coordinates
						if (role["office"].stringValue.contains("Longworth"))
						{
							office.address   	= OFFICE_BUILDINGS["LONGWORTH", "address", "street"].stringValue
							office.zipcode   	= OFFICE_BUILDINGS["LONGWORTH", "address", "zip"].stringValue
							office.latitude  	= OFFICE_BUILDINGS["LONGWORTH", "coordinates", "latitude"].doubleValue
							office.longitude	= OFFICE_BUILDINGS["LONGWORTH", "coordinates", "longitude"].doubleValue
						} else if (role["office"].stringValue.contains("Cannon"))
						{
							office.address   	= OFFICE_BUILDINGS["CANNON", "address", "street"].stringValue
							office.zipcode   	= OFFICE_BUILDINGS["CANNON", "address", "zip"].stringValue
							office.latitude  	= OFFICE_BUILDINGS["CANNON", "coordinates", "latitude"].doubleValue
							office.longitude	= OFFICE_BUILDINGS["CANNON", "coordinates", "longitude"].doubleValue
						} else if (role["office"].stringValue.contains("Rayburn"))
						{
							office.address   	= OFFICE_BUILDINGS["RAYBURN", "address", "street"].stringValue
							office.zipcode   	= OFFICE_BUILDINGS["RAYBURN", "address", "zip"].stringValue
							office.latitude  	= OFFICE_BUILDINGS["RAYBURN", "coordinates", "latitude"].doubleValue
							office.longitude	= OFFICE_BUILDINGS["RAYBURN", "coordinates", "longitude"].doubleValue
						}
					} else
					{
						if (role["office"].stringValue.contains("Russel"))
						{
							office.address   	= OFFICE_BUILDINGS["RUSSEL", "address", "street"].stringValue
							office.zipcode   	= OFFICE_BUILDINGS["RUSSEL", "address", "zip"].stringValue
							office.latitude  	= OFFICE_BUILDINGS["RUSSEL", "coordinates", "latitude"].doubleValue
							office.longitude	= OFFICE_BUILDINGS["RUSSEL", "coordinates", "longitude"].doubleValue
						} else if (role["office"].stringValue.contains("Hart"))
						{
							office.address   	= OFFICE_BUILDINGS["HART", "address", "street"].stringValue
							office.zipcode   	= OFFICE_BUILDINGS["HART", "address", "zip"].stringValue
							office.latitude  	= OFFICE_BUILDINGS["HART", "coordinates", "latitude"].doubleValue
							office.longitude	= OFFICE_BUILDINGS["HART", "coordinates", "longitude"].doubleValue
						} else if (role["office"].stringValue.contains("Dirksen"))
						{
							office.address   	= OFFICE_BUILDINGS["DIRKSEN", "address", "street"].stringValue
							office.zipcode   	= OFFICE_BUILDINGS["DIRKSEN", "address", "zip"].stringValue
							office.latitude  	= OFFICE_BUILDINGS["DIRKSEN", "coordinates", "latitude"].doubleValue
							office.longitude	= OFFICE_BUILDINGS["DIRKSEN", "coordinates", "longitude"].doubleValue
						}
					}
					office.officeID			= "\(member.apiID!)-washington"
					office.phoneNumber 		= role["phone"].stringValue
					office.state 			= "DC"
					office.officeOf 			= member
					member.billsSponsored 		= Int16(role["bills_sponsored"].intValue)
					member.billsCoSponsored 	= Int16(role["bills_cosponsored"].intValue)
					member.addToOffices(office)
					// set update flag so this request isn't made on every selection
					member.infoIsUpToDate = true
					CDManager.saveContext()
				}
			}
		}
	}
	
	/**
	Retrieve and store all current committees in Congress
	for the specified chamber.
	
	- parameters:
		- chamber: Congressional chamber committees to request.
	Possible values are `SENATE` or `HOUSE`
	
		- cascade: Whether to request the next chamber. `cascade` defaults to `false`.
	
		- joint: Whether to request joint committees. `joint` defaults to `false`.
	
	- Note: If `cascade` is `true` and a request for `SENATE` is
	made then a recursive `HOUSE` call will be made upon completion of the first call
	(and vice-versa). If `cascade` is `true` then joint committees will be requested.
	*/
	class func requestAllCommitteesFor(chamber: String, cascade: Bool = false, joint: Bool = false)
	{
		var requestString = ""
		if (joint) {
			requestString = RequestStringGenerator.craftRequestForCommitteesIn(chamber, joint: joint, congress: CURRENT_CONGRESS)
		} else {
			requestString = RequestStringGenerator.craftRequestForCommitteesIn(chamber)
		}
		
		CDManager.persistentContainer.performBackgroundTask()
			{ _ in
			Alamofire.request(requestString, headers: requestHeader).responseJSON { response in
				if let responseJSON = response.result.value
				{
					let json = JSON(responseJSON)
					let committees = json["results", 0, "committees"].arrayValue
					for committee in committees
					{
						let cdCommittee = Committee(context: CDManager.context())
						cdCommittee.apiURI = committee["api_uri"].stringValue
						cdCommittee.chairID = committee["chair_id"].stringValue
						cdCommittee.chamber = committee["chamber"].stringValue
						cdCommittee.committeeID = committee["id"].stringValue
						cdCommittee.committeeWebsite = committee["url"].stringValue
						cdCommittee.name = committee["name"].stringValue
						cdCommittee.rankingMemberID = committee["ranking_member_id"].stringValue
						cdCommittee.type = "Committee"
						let subcommittees = committee["subcommittees"].arrayValue
						for subcommittee in subcommittees
						{
							let subcom = Subcommittee(context: CDManager.context())
							subcom.apiURI = subcommittee["api_uri"].stringValue
							subcom.chamber = committee["chamber"].stringValue
							subcom.name = subcommittee["name"].stringValue
							subcom.committeeID = subcommittee["id"].stringValue
							subcom.type = "Subcommittee"
							subcom.parent = cdCommittee
							cdCommittee.addToSubcommittees(subcom)
						}
					}
					CDManager.saveContext()
					switch chamber {
					case HOUSE:
						if (cascade) {
							requestAllCommitteesFor(chamber: SENATE)
							requestAllCommitteesFor(chamber: JOINT_ENDPOINT, cascade: false, joint: true)
						}
					case SENATE:
						if (cascade) {
							requestAllCommitteesFor(chamber: HOUSE)
							requestAllCommitteesFor(chamber: JOINT_ENDPOINT, cascade: false, joint: true)
						}
					default: break
					}
				}
			} // outter Alfire request
		} // CONTAINER end
	} // requestAllCommitteesFor()

	class func updateCommitteeInfo() {
		CDManager.persistentContainer.performBackgroundTask() { _ in
			do {
				let request:NSFetchRequest<Committee> = Committee.fetchRequest()
				let fcs = try CDManager.context().fetch(request)
				for fc in fcs {
					Alamofire.request(fc.apiURI!, headers: requestHeader).responseJSON { response in
						if let responseJSON = response.result.value {
							let json = JSON(responseJSON)
							let results = JSON(json["results", 0].dictionaryObject!)
							let chairID = results["chair_id"].stringValue
							let rankingID = results["ranking_member_id"].stringValue
							let currentMembers = results["current_members"].arrayValue
							for member in currentMembers
							{
								let id = member["id"].stringValue
								do
								{
									let memRequest:NSFetchRequest<CongressMember> = CongressMember.fetchRequest()
									let predicate = NSPredicate(format: "apiID = %@", "\(id)")
									memRequest.predicate = predicate
									let frm = try CDManager.context().fetch(memRequest)[0]
									if (frm.apiID == chairID) {
										fc.chairID = frm.apiID
									} else if (frm.apiID == rankingID) {
										fc.rankingMemberID = frm.apiID
									}
									fc.addToMembers(frm)
									frm.addToMemberOf(fc)
								} catch { fatalError("Could not fetch member from CD \(error)") }
							}
							CDManager.saveContext()
						}
					}
				}
			} catch { fatalError("Couldn't not fetch committes \(error)") }
		}
	}
	
	class func getLegislation(memberID: String, billType: String, billsUpdate: Bool = false, cascade: Bool = false) {
		let dateFormatter = DateFormatter()
		dateFormatter.dateStyle = .short
		dateFormatter.dateFormat = "YYYY-MM-DD"
		CDManager.persistentContainer.performBackgroundTask() { _ in
			let request = RequestStringGenerator.craftRequestFromBills(by: memberID, billType: billType)
			
			Alamofire.request(request, headers: requestHeader).responseJSON { response in
				if let responseJSON = response.result.value {
					let json = JSON(responseJSON)
					var results = JSON(json["results", 0].dictionaryValue)
					let id = results["id"].stringValue
					let bills = results["bills"].arrayValue
					for bill in bills {
						let billEntity = Bill(context: CDManager.context())
						billEntity.active = bill["active"].boolValue
						billEntity.billID = bill["bill_id"].stringValue
						billEntity.billNumber = bill["number"].stringValue
						billEntity.billType = bill["bill_type"].stringValue
						billEntity.billURI = bill["bill_uri"].stringValue
						billEntity.congress = Int16(bill["congress"].intValue)
						let cosponsorsByPrty = JSON(bill["cosponsors_by_party"].dictionaryValue)
						billEntity.dCosponsors = Int16(cosponsorsByPrty["D"].intValue)
						
						// Get all the dates
						var dateString = bill["enacted"].stringValue
						billEntity.enactedOn = dateFormatter.date(from: dateString)
						
						dateString = bill["introduced_date"].stringValue
						billEntity.introducedOn = dateFormatter.date(from: dateString)
						
						dateString = bill["latest_major_action_date"].stringValue
						billEntity.lastMajorActionOn = dateFormatter.date(from: dateString)
						
						dateString = bill["last_vote"].stringValue
						billEntity.lastVote = dateFormatter.date(from: dateString)
						
						dateString = bill["house_passage"].stringValue
						billEntity.passedByHouseOn = dateFormatter.date(from: dateString)
						
						dateString = bill["senate_passge"].stringValue
						billEntity.passedBySenateOn = dateFormatter.date(from: dateString)
						
						billEntity.govtrackURL = bill["govtrack_url"].stringValue
						billEntity.iCosponsors = Int16(cosponsorsByPrty["I"].intValue)
						billEntity.lastMajorAction = bill["last_major_action"].stringValue
						billEntity.rCosponsors = Int16(cosponsorsByPrty["R"].intValue)
						billEntity.shortSummary = bill["summary_short"].stringValue
						billEntity.shortTitle = bill["short_title"].stringValue
						// is the requesting member also the sponsor of this bill?
						if (bill["sponsor_id"].stringValue == id){
							billEntity.sponsorID = id
						} else {
							billEntity.sponsorID = bill["sponsor_id"].stringValue
						}
						billEntity.summary = bill["summary"].stringValue
						billEntity.title = bill["title"].stringValue
					}
					CDManager.saveContext()
				}
				switch billType {
				case BILLS_INTRODUCED:
					if (cascade) { getLegislation(memberID: memberID, billType: BILLS_COSPONSORED) }
				case BILLS_COSPONSORED:
					if (cascade) { getLegislation(memberID: memberID, billType: BILLS_INTRODUCED) }
				default:break
				}
			}
		}
	}
	
	class func fetchFOD(date: Date) {
		let url = "http://www.historynet.com/today-in-history"
		var bornOnSection = false
		Alamofire.request(url).responseString
			{ responseString in
			if let html = responseString.result.value
			{
				do
				{
					let doc: Document = try! SwiftSoup.parse(html)
					let table: Element = try! (doc.select("table").first())!
					let tableRows: [Element] = try table.select("tr").array()
					
					for row in tableRows {
						let fod = FOD(context: CDManager.context())
						let cols = try row.select("td")
						var text = try cols.text()
						if (text.contains("Born on")) {
							bornOnSection = true
							// reset the string to skip over "Born on Month Date" title
							text = ""
						}
						
						if (bornOnSection)
						{
							// make sure it's not the empty string I set
							if (text != "")
							{
								fod.dateOfFact = date
								fod.fact = text
								fod.isBornOnFact = true
							}
						}
						else
						{
							fod.dateOfFact = date
							fod.fact = text
							fod.isBornOnFact = false
						}
					}
					CDManager.saveContext()
				}
				catch Exception.Error( _, let message) { print(message) }
				catch { print("There was a problem getting On this Date in History \(error)") }
			}
		}
	}
} // class end
