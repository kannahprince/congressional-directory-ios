//
//  Utilities.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 12/12/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import CoreData

/**
The ID of the previously displayed `FOD`.

- ToDo: Use to prevent repetion of displaying
the same `FOD` more than once.
*/
private var previousID: NSManagedObjectID!

/**
The `Utilities` class is a library of general purpose,
reusable helper functions.

- Author:
	Prince Kannah
*/
class Utilities: NSObject {
	
	/**
	Retrieve the full name of the specified state abbreviation.
	- Parameter stateAbbreviation: The abbreviation of the state
	name to retrieve.
	- Returns: The full state name of the specified abbreviation.
	Returns `nil` when `stateAbbreviaiton`	is invalid.
	*/
	class func reverseLookupFor(stateAbbreviation: String)  -> String? {
		for key in STATE_DICT
		{
			if (key.value == stateAbbreviation) {
				return key.key
			}
		}
		return nil
	}
	
	/**
	Retrieve the two letter state abbreviation for the specified
	state name
	- Parameter stateName: The full name of the state to retrieve
	the initials of.
	- Returns: The two letter state abbreviation of the specified
	state name. Returns `nil` when `stateName` is invalid.
	*/
	class func stateAbbreviationFor(stateName: String) -> String? {
		for key in STATE_DICT
		{
			if (key.key == stateName)
			{
				return key.value
			}
		}
		return nil
	}
	
	/**
	Retrieve a fact of the day.
	- Returns: A pretty fact of the day string. Returns `nil`
	if there is no fact of the day 😕.
	*/
	class func factOfTheDay() -> String? {
		do {
			let request: NSFetchRequest<FOD> = FOD.fetchRequest()
			let lastDate = UserDefaults.standard.value(forKey: FOD_FETCHED) as! NSDate
			let predicate = NSPredicate(format: "dateOfFact ==%@", lastDate)
			request.predicate = predicate
			let resuts = try CDManager.context().fetch(request)
			
			if (resuts.count > 0)
			{
				let index = Int(arc4random_uniform(UInt32(resuts.count)))
				let selectedFOD = resuts[index]
				previousID = selectedFOD.objectID
				
				let selectedText = selectedFOD.fact!
				// incase we get a default string
				if (selectedText == "FACT") { return nil }
				
				let yearIndex = selectedText.index(of: " ")!
				switch selectedFOD.isBornOnFact
				{
				case true:
					let clean = selectedText[yearIndex...].trimmingCharacters(in: .whitespaces).replacingOccurrences(of: ".", with: "")
					let cleanYear = selectedText[...yearIndex].trimmingCharacters(in: .whitespaces)
					let str = """
					Born today:
					In \(cleanYear), \(clean).
					"""
					return str
				default:
					let clean = selectedText[yearIndex...].trimmingCharacters(in: .whitespaces)
					let cleanYear = selectedText[...yearIndex].trimmingCharacters(in: .whitespaces)
					let str = """
					Todo in history:
					In \(cleanYear), \(clean)
					"""
					return str
				}
			}
		} catch {
			fatalError("Could not fetch FOD")
		}
		return nil
	}
}
