//
//  RepresentativeCellView.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/17/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import UIKit

let CELL_IDENTIFIER = "rep"

class RepresentativeCellView: UITableViewCell {
	@IBOutlet weak var repImage: UIImageView!
	@IBOutlet weak var name: UILabel!
    @IBOutlet weak var party: UILabel!
}
