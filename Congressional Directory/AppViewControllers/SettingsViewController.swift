//
//  SettingsViewController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/15/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import UIKit

class SettingsViewController: UIViewController, UITextFieldDelegate {
	@IBOutlet weak var currentStateLabel: UILabel!
	@IBOutlet weak var homeStateInput: UITextField!
	
	@IBAction func saveBtnAction(_ sender: Any) {
		if (homeStateInput.text != nil) {
			let state = homeStateInput.text!.trimmingCharacters(in: .whitespaces)
			let stateAbrev = Utilities.stateAbbreviationFor(stateName: state)
			if (stateAbrev != nil)
			{
				currentStateLabel.text = "Current state: \(state)"
				UserDefaults.standard.setValue(stateAbrev, forKey: STATE_ABREV)
				UserDefaults.standard.set(true, forKey: STATE_IS_SET)
				performSegue(withIdentifier: "toMyRep", sender: nil)
			}
		}
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		self.view.endEditing(true)
		return true
	}
	
	override func viewDidLoad() {
		if (UserDefaults.standard.bool(forKey: STATE_IS_SET)) {
			let stateAbrev = UserDefaults.standard.value(forKey: STATE_ABREV) as! String
			let state = Utilities.reverseLookupFor(stateAbbreviation: stateAbrev)
			if (state != nil) {
				currentStateLabel.text = "Current state: \(state!)"
			}
		}
    }
}
