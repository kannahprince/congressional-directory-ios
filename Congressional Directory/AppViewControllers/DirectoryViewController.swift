//
//  DirectoryViewController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/15/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DirectoryViewController: UITableViewController, UISearchBarDelegate, NSFetchedResultsControllerDelegate {
	static let CELL_IDENTIFIER = "search"
	var isUsingScope = false
	let sectionSort = NSSortDescriptor(key: "title", ascending: true)
	let firstNameSort = NSSortDescriptor(key: "firstName", ascending: true)
	var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
	
	var sectionTitles = ["Representatives", "Senators"]
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = "Search"
		initFRC()
	}
	
	func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
		switch selectedScope {
		case 1:
			if(searchBar.text?.isEmpty)! {
				fetchedResultsController = nil
				let predicate = NSPredicate(format: "party = %@", "R")
				initFRC(usePredicate: true, predicate: predicate)
				tableView.reloadData()
			} else {
				searchBarSearchButtonClicked(searchBar)
			}
		case 2:
			if(searchBar.text?.isEmpty)! {
				fetchedResultsController = nil
				let predicate = NSPredicate(format: "party = %@", "D")
				initFRC(usePredicate: true, predicate: predicate)
				tableView.reloadData()
			} else {
				searchBarSearchButtonClicked(searchBar)
			}
		case 3:
			if(searchBar.text?.isEmpty)! {
				isUsingScope = true
				fetchedResultsController = nil
				let predicate = NSPredicate(format: "title = %@", "Senator")
				initFRC(usePredicate: true, predicate: predicate)
				tableView.reloadData()
			} else {
				searchBarSearchButtonClicked(searchBar)
			}
		case 4:
			if(searchBar.text?.isEmpty)! {
				fetchedResultsController = nil
				let predicate = NSPredicate(format: "title = %@", "Representative")
				initFRC(usePredicate: true, predicate: predicate)
				tableView.reloadData()
			} else {
				searchBarSearchButtonClicked(searchBar)
			}
		// Display all members from both chambers
		default:
			if(searchBar.text?.isEmpty)! {
				fetchedResultsController = nil
				initFRC()
				tableView.reloadData()
			} else {
				searchBarSearchButtonClicked(searchBar)
			}
		}
		isUsingScope = false
	}
	
	func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
		// reset the selected index to All
		// so all results are displayed when a search is complete
		searchBar.selectedScopeButtonIndex = 0
	}
	
	// MARK: - cancel out of search results
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		// Hide keyboard
		self.view.endEditing(true)
		searchBar.text = ""
		searchBar.selectedScopeButtonIndex = 0
		// repopulate table with ALL members
		initFRC()
		tableView.reloadData()
	}
	
	// MARK: - search button clicked
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		if (searchBar.text?.isEmpty)! { return }
		
		// Get search query
		let query = searchBar.text!
		let trimmedQuery = query.trimmingCharacters(in: .whitespaces)
		// Is query a state name?
		let stateValidation = isAValidState(trimmedQuery)
		
		if (stateValidation.isValid) {
			// valid state -> search by state name
			searchByState(searchBar, stateValidation.stateAbbreviation)
		} else {
			let nameArray = trimmedQuery.components(separatedBy: " ")
			// we have first and last name -> search exactly
			if (nameArray.count >= 2){
				let namePredicate = NSPredicate(format: "firstName = %@ AND lastName = %@", nameArray[0], nameArray[1])
				initFRC(usePredicate: true, predicate: namePredicate)
			} else if (nameArray.count <= 1){
				searchByName(searchBar, nameArray)
			}
		}
		tableView.reloadData()
		self.view.endEditing(true)
		isUsingScope = false
	}
	
	// MARK: - search by state
	func searchByState(_ searchBar: UISearchBar, _ stateAbbreviation: String) {
		let statePredicate = NSPredicate(format: "stateAbbreviation = %@", stateAbbreviation)
		switch searchBar.selectedScopeButtonIndex {
		case 1:
			let predicate = NSPredicate(format: "stateAbbreviation = %@ AND party = %@", stateAbbreviation, "R")
			initFRC(usePredicate: true, predicate: predicate)
		case 2:
			let predicate = NSPredicate(format: "stateAbbreviation = %@ AND party = %@", stateAbbreviation, "D")
			initFRC(usePredicate: true, predicate: predicate)
		case 3:
			isUsingScope = true
			let predicate = NSPredicate(format: "stateAbbreviation = %@ AND title = %@", stateAbbreviation, "Senator")
			initFRC(usePredicate: true, predicate: predicate)
		case 4:
			let predicate = NSPredicate(format: "stateAbbreviation = %@ AND title = %@", stateAbbreviation, "Representative")
			initFRC(usePredicate: true, predicate: predicate)
		// Display all members from both chambers
		default:
			initFRC(usePredicate: true, predicate: statePredicate)
		}
	}
	
	// MARK: - search by name
	func searchByName (_ searchBar: UISearchBar, _ nameArray: [String]) {
		// we have first OR last name -> search by either
		let namePredicate = NSPredicate(format: "firstName = %@ OR lastName = %@", nameArray[0], nameArray[0])
		// initializeFetchedResultsController(usePredicate: true, predicate: namePredicate)
		// narrow down results
		switch searchBar.selectedScopeButtonIndex {
		case 1:
			let partyPredicate = NSPredicate(format: "party = %@", "R")
			let predicates = NSCompoundPredicate(andPredicateWithSubpredicates: [namePredicate, partyPredicate])
			initFRCWithCompoundPredicate(predicate: predicates)
		case 2:
			let partyPredicate = NSPredicate(format: "party = %@", "D")
			let predicates = NSCompoundPredicate(andPredicateWithSubpredicates: [namePredicate, partyPredicate])
			initFRCWithCompoundPredicate(predicate: predicates)
		case 3:
			isUsingScope = true
			let chamberPredicate = NSPredicate(format: "title = %@", "Senator")
			let predicates = NSCompoundPredicate(andPredicateWithSubpredicates: [namePredicate, chamberPredicate])
			initFRCWithCompoundPredicate(predicate: predicates)
		case 4:
			let chamberPredicate = NSPredicate(format: "title = %@", "Representative")
			let predicates = NSCompoundPredicate(andPredicateWithSubpredicates: [namePredicate, chamberPredicate])
			initFRCWithCompoundPredicate(predicate: predicates)
		// display all members matching first OR last names
		default:
			initFRC(usePredicate: true, predicate: namePredicate)
		}
	}
	
	// MARK: - check the specified name is valid
	// and return its corresponding abbreviation
	func isAValidState (_ stateName: String) -> (isValid: Bool, stateAbbreviation: String) {
		for key in STATE_DICT.keys {
			if (key == stateName){
				return (true, STATE_DICT[key]!)
			}
		}
		return (false, "")
	}
	
	// MARK: - initialize the FetchResultsController
	func initFRC(usePredicate: Bool = false, predicate: NSPredicate = NSPredicate()) {
		let request = NSFetchRequest<NSFetchRequestResult>(entityName: MEMBER_ENTITY)
		request.sortDescriptors = [sectionSort, firstNameSort]
		if (usePredicate) {
			request.predicate = predicate
		}
		
		let moc = CDManager.context()
		fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: "title", cacheName: nil)
		fetchedResultsController.delegate = self
		
		do {
			try fetchedResultsController.performFetch()
		} catch {
			fatalError("Failed to initialize FetchedResultsController: \(error)")
		}
	}
	
	// MARK: - initialize the FetchResultsController with a compound predicate
	func initFRCWithCompoundPredicate(predicate: NSCompoundPredicate) {
		let request = NSFetchRequest<NSFetchRequestResult>(entityName: MEMBER_ENTITY)
		request.sortDescriptors = [sectionSort, firstNameSort]
		request.predicate = predicate
		
		let moc = CDManager.context()
		fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: "title", cacheName: nil)
		fetchedResultsController.delegate = self
		
		do {
			try fetchedResultsController.performFetch()
		} catch {
			fatalError("Failed to initialize FetchedResultsController: \(error)")
		}
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		// Set up the cell
		let cell = tableView.dequeueReusableCell(withIdentifier: DirectoryViewController.CELL_IDENTIFIER, for: indexPath) as! RepresentativeCellView
		guard let object = self.fetchedResultsController?.object(at: indexPath) else {
			fatalError("Attempt to configure cell without a managed object")
		}
		
		let member = object as! CongressMember
		cell.name.text = member.firstName! + " " + member.middleName! + " " + member.lastName!
		cell.repImage.sd_setImage(with: URL(string: IMAGE_BASE_URL + member.apiID! + ".jpg"), placeholderImage: UIImage(named: "placeholder.png"))
		
		if (member.title == "Representative") {
			let rep = member as! Representative
			if(member.party == "R"){
				if(rep.districtAtLarge){
					cell.party.text = "R," + " (District \(DISTRICT_AT_LARGE), \(member.stateAbbreviation!))"
				} else {
					cell.party.text = "R," + " (District \(rep.district), \(member.stateAbbreviation!))"
				}
			}else{
				if(rep.districtAtLarge){
					cell.party.text = "D," + " (District \(DISTRICT_AT_LARGE), \(member.stateAbbreviation!))"
				} else {
					cell.party.text = "D," + " (District \(rep.district), \(member.stateAbbreviation!))"
				}
			}
		} else {
			if(member.party == "R"){
				cell.party.text = "R, \(member.stateAbbreviation!)"
			}else{
				cell.party.text = "D, \(member.stateAbbreviation!)"
			}
		}
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let selectedMember = fetchedResultsController.object(at: indexPath) as! CongressMember
		SELECTED_MEMBER_ID = selectedMember.apiID!
		if (!selectedMember.infoIsUpToDate) {
			DataFetcher.updateInfoFor(member: selectedMember)
			DataFetcher.getLegislation(memberID: SELECTED_MEMBER_ID, billType: BILLS_INTRODUCED, cascade: true)
		}
	}
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		// TODO: return Representative if there is only one
		return sectionTitles[section]
	}
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return fetchedResultsController.sections!.count
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let sections = fetchedResultsController.sections else {
			fatalError("No sections in fetchedResultsController")
		}
		let sectionInfo = sections[section]
		let numObjects = sectionInfo.numberOfObjects
		
		if (section == 0) {
			if (isUsingScope) {
				if (numObjects <= 1){
					sectionTitles[0] = "Senator"
				} else {
					sectionTitles[0] = "Senators"
				}
			} else {
				// don't worry about scope
				if (numObjects <= 1) {
					sectionTitles[0] = "Representative"
				} else {
					sectionTitles[0] = "Representatives"
				}
			}
		} else {
			if (numObjects <= 1) {
				sectionTitles[1] = "Senator"
			} else {
				sectionTitles[1] = "Senators"
			}
		}
		return sectionInfo.numberOfObjects
	}
}
