//
//  AboutViewController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/15/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import UIKit

class AboutViewController: UIViewController {
//	let PRO_PUBLICA_DISPLAY_STRING = "ProPublica Congress API"
//	let US_PROJECT_DISPLAY_STRING = "United States Project"
	@IBOutlet weak var textView: UITextView!
	
	override func viewDidLoad() {
		textView.text = """
		The data presented in this app is compiled from the ProPublica Congress API as well as the United States Project.
		
		Images and biography are sourced from the Biographical Directory of the United States Congress.
		
		Data update schedule:
		⦿ Bill data, including sponsorships, subjects and amendments, is updated six times a day at the following times: 7:00 a.m., 9:45 a.m., 12:45 p.m., 4:45 p.m., 9:45 p.m. and 1:45 a.m. (All Eastern Standard Time)
		
		⦿ House and Senate votes, along with House and Senate floor actions, are updated every 30 minutes.
		
		⦿ Nomination data and committee hearing data is updated at 12 p.m. every day.
		
		⦿ Data on personal explanations and leaves of absence is updated at 12 p.m. every day.
		
		
		
				        Made with ♥️ in Boise, ID
		"""
    }
}
