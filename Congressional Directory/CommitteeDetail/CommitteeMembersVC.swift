//
//  MembersCollectionViewController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 12/8/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit
import SDWebImage
import CoreData

private let reuseIdentifier = "member"

class CommitteeMembersVC: UICollectionViewController, NSFetchedResultsControllerDelegate {
	var frc: NSFetchedResultsController<NSFetchRequestResult>!
	override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
		// Do any additional setup after loading the view.
		initializeFetchedResultsController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return frc.sections!.count
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		guard let sections = frc.sections else {
			fatalError("[MembersCollectionVC]No sections in fetchedResultsController")
		}
		let sectionInfo = sections[section]
		return sectionInfo.numberOfObjects
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MemberCollectionViewCell
		guard let object = self.frc?.object(at: indexPath) else {
			fatalError("[MembersCollectionVC]Attempt to configure cell without a managed object")
		}
		let member = object as! CongressMember
		cell.imageView.sd_setImage(with: URL(string: IMAGE_BASE_URL + member.apiID! + ".jpg"), placeholderImage: UIImage(named: "placeholder.png"))
		cell.label.text = member.lastName! + " (\(member.party!)-\(member.stateAbbreviation!))"
        return cell
    }
	
	override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let selected = frc.object(at: indexPath) as! CongressMember
		SELECTED_MEMBER_ID = selected.apiID!
		if (!selected.infoIsUpToDate) {
			DataFetcher.updateInfoFor(member: selected)
		}
	}
	
	func initializeFetchedResultsController() {
		let request = NSFetchRequest<NSFetchRequestResult>(entityName: MEMBER_ENTITY)
		let lastNameSort = NSSortDescriptor(key: "lastName", ascending: true)
		let predicate = NSPredicate(format: "ANY memberOf.committeeID = %@", "\(SELECTED_COMMITTE_ID)")
		request.sortDescriptors = [lastNameSort]
		request.predicate = predicate
		
		let moc = CDManager.context()
		frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
		frc.delegate = self
		do {
			try frc.performFetch()
		} catch {
			fatalError("[MembersCollectionVC]Failed to initialize FetchedResultsController: \(error)")
		}
	}
	
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */
}
