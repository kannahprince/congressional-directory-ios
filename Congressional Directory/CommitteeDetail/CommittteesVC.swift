//
//  CommittteesVC.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/21/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CommitteesVC: UITableViewController, NSFetchedResultsControllerDelegate {
	var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		if (!UserDefaults.standard.bool(forKey: COMMITTEES_MEMBERSHIP)) {
			DataFetcher.updateCommitteeInfo()
			UserDefaults.standard.set(true, forKey: COMMITTEES_MEMBERSHIP)
		}
		initializeFetchedResultsController()
		tableView.tableFooterView = UIView()
	}
	
	func initializeFetchedResultsController() {
		let request = NSFetchRequest<NSFetchRequestResult>(entityName: COMMITTEE_ENTITY)
		let selectBaseOn = NSSortDescriptor(key: "committeeID", ascending: true)
		// display only Committees
		let predicate = NSPredicate(format: "type = %@", "Committee")
		request.sortDescriptors = [selectBaseOn]
		request.predicate = predicate
		let moc = CDManager.context()
		fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: "chamber", cacheName: nil)
		fetchedResultsController.delegate = self
		
		do {
			try fetchedResultsController.performFetch()
		} catch {
			fatalError("Failed to initialize FetchedResultsController: \(error)")
		}
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "committees", for: indexPath)
		// Set up the cell
		guard let object = self.fetchedResultsController?.object(at: indexPath) else {
			fatalError("Attempt to configure cell without a managed object")
		}
		let committee = object as! Committee
		cell.textLabel?.text = committee.name
		//Populate the cell from the object
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let selectedCommittee = fetchedResultsController.object(at: indexPath) as! Committee
		SELECTED_COMMITTE_ID = selectedCommittee.committeeID!
	}
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		guard let sections = fetchedResultsController.sections else {
			fatalError("[SubcommitteeTVC]No sections in fetchedResultsController")
		}
		let sectionInfo = sections[section]
		return sectionInfo.name
	}
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return fetchedResultsController.sections!.count
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let sections = fetchedResultsController.sections else {
			fatalError("No sections in fetchedResultsController")
		}
		let sectionInfo = sections[section]
		return sectionInfo.numberOfObjects
	}
}
