//
//  CommitteeTabBarControllerVC.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 12/8/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit

class CommitteeTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
		let com:Committee = CDManager.fetchCommittee(SELECTED_COMMITTE_ID)
		self.title = " " + com.name!
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
