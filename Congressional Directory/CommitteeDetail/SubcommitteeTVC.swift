//
//  SubcommitteeTableViewController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 12/8/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit
import CoreData



class SubcommitteeTVC: UITableViewController, NSFetchedResultsControllerDelegate {
	private let CELL_ID = "Subcommittees"
	var frc:NSFetchedResultsController<NSFetchRequestResult>!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		initializeFetchedResultsController()
		tableView.tableFooterView = UIView()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return frc.sections!.count
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let sections = frc.sections else {
			fatalError("No sections in fetchedResultsController")
		}
		let sectionInfo = sections[section]
		return sectionInfo.numberOfObjects
	}

	func initializeFetchedResultsController() {
		let request = NSFetchRequest<NSFetchRequestResult>(entityName: SUBCOMMITTEE_ENTITY)
		let selectBaseOn = NSSortDescriptor(key: "committeeID", ascending: true)
		// display only Committees
		let predicate = NSPredicate(format: "ANY parent.committeeID = %@", "\(SELECTED_COMMITTE_ID)")
		request.sortDescriptors = [selectBaseOn]
		request.predicate = predicate
		let moc = CDManager.context()
		frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: "type", cacheName: nil)
		frc.delegate = self
		do {
			try frc.performFetch()
		} catch {
			fatalError("[SubcommitteeTVC]Failed to initialize FetchedResultsController: \(error)")
		}
	}
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		guard let sections = frc.sections else {
			fatalError("[SubcommitteeTVC]No sections in fetchedResultsController")
		}
		let sectionInfo = sections[section]
		return sectionInfo.name
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID, for: indexPath)
		guard let object = frc?.object(at: indexPath) else {
			fatalError("[ContactsTVC]Attempt to configure cell without a managed object")
		}
		// Configure the cell...
		let com = object as! Subcommittee
		cell.textLabel?.text = com.name
		return cell
	}
}
