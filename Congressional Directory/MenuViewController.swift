//
//  MenuViewController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 12/6/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit
import CoreLocation

class MenuViewController: UIViewController, UITextFieldDelegate {
	let defaultText = "Welcome to Congressional Directory. Open the menu to see your list of representative and explore!"
	var menuIsOpen = false
	var isVertical = false
	@IBOutlet weak var textView: UITextView!
	@IBOutlet weak var topView: UIView!
	@IBOutlet weak var trailingConstraint: NSLayoutConstraint!
	@IBOutlet weak var leadingConstraint: NSLayoutConstraint!
	
	override func viewDidLoad() {
        super.viewDidLoad()
		topView.layer.shadowOpacity = 1
		topView.layer.shadowRadius = 10
		updateFOD()
    }

	@IBAction func nextFODBtnAction(_ sender: Any) {
		updateFOD()
	}
	
	func updateFOD() {
		let fod = Utilities.factOfTheDay()
		if (fod != nil) {
			textView.text = fod
		}
	}
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	@IBAction func showMenu(_ sender: Any) {
		if(menuIsOpen)
		{
			leadingConstraint.constant = 0
			trailingConstraint.constant = 0
		} else
		{
			leadingConstraint.constant = 190
			trailingConstraint.constant = -190
		}
		UIView.animate(withDuration: 0.3, animations:
			{
			if (self.isVertical)
			{
				self.navigationItem.leftBarButtonItem?.image = UIImage(named: "menuSquared")
			} else
			{
				self.navigationItem.leftBarButtonItem?.image = UIImage(named: "menuSquaredFilled")
			}
			self.view.layoutIfNeeded()
		})
		menuIsOpen = !menuIsOpen
		isVertical = !isVertical
	}
	
	override func viewDidAppear(_ animated: Bool) {
		if (!UserDefaults.standard.bool(forKey: STATE_IS_SET))
		{
			showAskForStateAlert()
		}
	}
	
	func showAskForStateAlert() {
		let alert = UIAlertController(title: "Where do you live?", message: "Please provide the name of your state or US territory", preferredStyle: UIAlertControllerStyle.alert)
		
		alert.addTextField { (textField: UITextField) in
			textField.keyboardType = .default
			textField.autocorrectionType = .yes
			textField.returnKeyType = .done
			// capitalize the first letter of each word.
			// helpful for multiple word state name (i.e New York)
			textField.autocapitalizationType = .words
			textField.placeholder = "Idaho"
			textField.clearButtonMode = .whileEditing
		}
		
		let submitAction = UIAlertAction(title: "Submit", style: .default, handler: { (action) -> Void in
			let state = alert.textFields![0].text?.trimmingCharacters(in: .whitespaces)
			// incase there's double tap on keybaord & period is added
			let cleanState = state?.replacingOccurrences(of: ".", with: "")
			let stateAbrev = Utilities.stateAbbreviationFor(stateName: cleanState!)
			if (stateAbrev != nil)
			{
				UserDefaults.standard.setValue(stateAbrev, forKey: STATE_ABREV)
				UserDefaults.standard.set(true, forKey: STATE_IS_SET)
				self.performSegue(withIdentifier: "toRepresentatives", sender: self)
			} else {
				self.showAskForStateAlert()
			}
		})
		
		alert.addAction(submitAction)
		self.present(alert, animated: true, completion: nil)
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		showAskForStateAlert()
		return true
	}
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
